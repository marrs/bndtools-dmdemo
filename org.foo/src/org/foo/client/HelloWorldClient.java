package org.foo.client;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.foo.HelloWorldService;

@Component
public class HelloWorldClient {
	@ServiceDependency(required = true)
	private volatile HelloWorldService m_service;
	
	@Start
	public void start() {
		m_service.greet("Marcel");
	}
}

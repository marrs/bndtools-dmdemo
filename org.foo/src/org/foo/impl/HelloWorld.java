package org.foo.impl;

import org.apache.felix.dm.annotation.api.Component;
import org.foo.HelloWorldService;

@Component(provides = HelloWorldService.class)
public class HelloWorld implements HelloWorldService {
	public void greet(String string) {
		System.out.println("Hello " + string);
	}
}

# BndTools based demo of DependencyManager annotations

This BndTools project shows the basics of using DependencyManager based annotations. The `org.foo` project contains three bundles: API, Client and Impl. The API bundle obviously contains the shared service API in an exported package. The Impl bundle implements the service and contains annotations that make sure this implementation is registered as a service. Finally, the Client bundle contains a component that consumes this service and invokes it.

The DependencyManager comes with a Bnd plugin to parse such annotations at compile time. A runtime component that can be deployed alongside the core DependencyManager bundle ensures these annotated components get picked up at runtime. The default run configuration also contains a shell with the normal 'dm' command which also nicely shows the runtime state of all dependencies.

Using those annotations requires the following steps:

1. Put the annotation processor jar in cnf/plugins.
2. Add the dependency manager core, runtime and optionally shell bundles to your repository.
3. Add a couple of lines to your projects bnd.bnd file to include the annotations and use the Bnd plugin to process the annotations.
